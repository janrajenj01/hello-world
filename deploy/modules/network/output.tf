output "vpc_id" {
  description = "ID of the VPC"
  value       = aws_vpc.main1.id
  }


  output "subnet_id_pri" {
    description = "subnets for eks cluster"
    value = [aws_subnet.private_a.id, aws_subnet.private_b.id]
  }

  output "subnet_id_pub" {
    description = "subnets for bastion node"
    value = [aws_subnet.public_a.id, aws_subnet.public_b.id]
  }
  output "security_group_id" {
    description = "bastion_node sg from to eks"
    value = aws_security_group.bastion2sg.id
  }

  
