resource "aws_vpc" "main1" {
  cidr_block           = "10.1.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "${var.env}-${var.project}-vpc"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"
  }
}

resource "aws_internet_gateway" "main2" {
  vpc_id = aws_vpc.main1.id

  tags = {
    Name = "${var.env}-${var.project}-igw"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"
  }

}

#public subnets inbound/outbound internet access

resource "aws_subnet" "public_a" {
  cidr_block              = "10.1.1.0/24"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.main1.id
  availability_zone       = "${data.aws_region.current.name}a"

  tags = {
    Name = "${var.env}-${var.project}-subnet-public-a"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"
  }
}

resource "aws_route_table" "public_a" {
  vpc_id = aws_vpc.main1.id

  tags = {
    Name = "${var.env}-${var.project}-rt-public-a"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"
  }

}

resource "aws_route_table_association" "public_a" {
  subnet_id      = aws_subnet.public_a.id
  route_table_id = aws_route_table.public_a.id
}

resource "aws_route" "public_internet_access_a" {
  route_table_id         = aws_route_table.public_a.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main2.id
}

resource "aws_eip" "public_a" {
  domain = "vpc"
  tags = {
    Name = "${var.env}-${var.project}-eip-public-a"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"

  }
}

resource "aws_nat_gateway" "public_a" {
  allocation_id = aws_eip.public_a.id
  subnet_id     = aws_subnet.public_a.id
  tags = {
    Name = "${var.env}-${var.project}-ng-public-a"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"

  }
  
}

resource "aws_subnet" "public_b" {
  cidr_block              = "10.1.2.0/24"
  vpc_id                  = aws_vpc.main1.id
  map_public_ip_on_launch = true
  availability_zone       = "${data.aws_region.current.name}b"

  tags = {
    Name = "${var.env}-${var.project}-subnet-public-b"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"
  }
}

resource "aws_route_table" "public_b" {
  vpc_id = aws_vpc.main1.id

  tags = {
    Name = "${var.env}-${var.project}-rt-public-b"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"
  }

}

resource "aws_route_table_association" "public_b" {
  subnet_id      = aws_subnet.public_b.id
  route_table_id = aws_route_table.public_b.id
}

resource "aws_route" "public_internet_access_b" {
  route_table_id         = aws_route_table.public_b.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main2.id

}

resource "aws_eip" "public_b" {
  domain = "vpc"
  tags = {
    Name = "${var.env}-${var.project}-eip-public-b"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"
  }
}

resource "aws_nat_gateway" "public_b" {
  allocation_id = aws_eip.public_b.id
  subnet_id     = aws_subnet.public_b.id

  tags = {
    Name = "${var.env}-${var.project}-ng-public-b"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"
  }
}



#create private subnets

resource "aws_subnet" "private_a" {
  cidr_block        = "10.1.10.0/24"
  vpc_id            = aws_vpc.main1.id
  availability_zone = "${data.aws_region.current.name}a"

  tags = {
    Name = "${var.env}-${var.project}-subnet-private-a"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"
    "kubernetes.io/role/internal-elb" = "1"
  }
}

resource "aws_route_table" "private_a" {
  vpc_id = aws_vpc.main1.id

  tags = {
    Name = "${var.env}-${var.project}-rt-private-a"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"
  }
}

  
resource "aws_route_table_association" "private_a" {
  subnet_id      = aws_subnet.private_a.id
  route_table_id = aws_route_table.private_a.id
}

resource "aws_route" "private_a_internet_out" {
  route_table_id         = aws_route_table.private_a.id
  nat_gateway_id         = aws_nat_gateway.public_a.id
  destination_cidr_block = "0.0.0.0/0"

}

resource "aws_subnet" "private_b" {
  cidr_block        = "10.1.11.0/24"
  vpc_id            = aws_vpc.main1.id
  availability_zone = "${data.aws_region.current.name}b"

  tags = {
    Name = "${var.env}-${var.project}-subnet-private-b"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"
    "kubernetes.io/role/internal-elb" = "1"
     
  }
  
}

resource "aws_route_table" "private_b" {
  vpc_id = aws_vpc.main1.id

  tags = {
    Name = "${var.env}-${var.project}-rt-private-b"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"
  }
}

resource "aws_route_table_association" "private_b" {
  subnet_id      = aws_subnet.private_b.id
  route_table_id = aws_route_table.private_b.id
}

resource "aws_route" "private_b_internet_out" {
  route_table_id         = aws_route_table.private_b.id
  nat_gateway_id         = aws_nat_gateway.public_b.id
  destination_cidr_block = "0.0.0.0/0"

}

###Bastion Node SG

resource "aws_security_group" "bastion2sg" {
  description = "control bastion inbound and outbound access"
  vpc_id      = aws_vpc.main1.id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]

  }
  tags = {
    Name       = "bastion2-sg"
    Env = "${var.env}"
    Project = "${var.project}"
    contact = "${var.contact}"
    ManagedBy = "${var.ManagedBy}"
  }

}
