variable "env" {
  default = "DEV"
}

variable "project" {
  default = "Sample-Java-Project"
}

variable "contact" {
  default = "jr00756572@techmahindra.com"
}

variable "ManagedBy" {
  default = "TerraformAdmin"
}


data "aws_region" "current" {}


