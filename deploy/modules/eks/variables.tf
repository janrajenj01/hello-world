variable "env" {
  default = "DEV"
}

variable "project" {
  default = "Sample-Java-Project"
}

variable "vpc_id" {}

variable "subnet_pri_ids" {}

variable "security_group_id_bastion_id" {
  description = "securitygroup from n/w  module"
  type = string 
}

variable "contact" {
  default = "jr00756572@techmahindra.com"
}

variable "ManagedBy" {
  default = "TerraformAdmin"
}

data "aws_region" "current" {}

