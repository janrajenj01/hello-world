output "eks-cluster" {
    description = "fetch eks-cluster-name"
    value = aws_eks_cluster.eks_cluster.name
}
