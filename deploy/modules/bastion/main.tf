resource "aws_instance" "bastion_node2" {
  ami                    = "ami-0230bd60aa48260c6"
  instance_type          = "t2.micro"
  key_name               = "bastion-node2-key"
  user_data = <<-EOF
  #!/bin/bash

  #install kubectl
  curl -O https://s3.us-west-2.amazonaws.com/amazon-eks/1.28.2/2023-10-17/bin/linux/amd64/kubectl
  chmod +x ./kubectl
  mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$HOME/bin:$PATH

  #Install helm
  curl -LO https://get.helm.sh/helm-v3.11.0-linux-amd64.tar.gz
  tar xfz helm-v3.11.0-linux-amd64.tar.gz
  sudo mv linux-amd64/helm /usr/local/bin/

  helm repo add eks https://aws.github.io/eks-charts

  #Install eksctl

  ARCH=amd64 
  PLATFORM=$(uname -s)_$ARCH  
  curl -sLO "https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_$PLATFORM.tar.gz" 
  tar -xzf eksctl_$PLATFORM.tar.gz -C /tmp && rm eksctl_$PLATFORM.tar.gz
  sudo mv /tmp/eksctl /usr/local/bin

  aws configure set aws_access_key_id "${var.access_key_id}"
  aws configure set aws_secret_access_key "${var.secret_key_id}"

  aws eks update-kubeconfig --region us-east-1 --name stg-MV-eks-cluster
EOF
  subnet_id              = "${var.subnet_pub_ids[0]}"
  vpc_security_group_ids = ["${var.security_group_id_bastion_id}"]

}
