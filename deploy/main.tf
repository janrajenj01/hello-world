data "aws_region" "current" {}

module "network" {
  source = "./modules/network"
}

module "eks" {
  source                       = "./modules/eks"
  vpc_id                       = module.network.vpc_id
  security_group_id_bastion_id = module.network.security_group_id
  subnet_pri_ids                   = module.network.subnet_id_pri
  depends_on                   = [module.network]
}

module "bastion" {
  source                       = "./modules/bastion"
  vpc_id                       = module.network.vpc_id
  security_group_id_bastion_id = module.network.security_group_id
  subnet_pub_ids                   = module.network.subnet_id_pub
  eks_cluster                  = module.eks.eks-cluster
  depends_on                   = [module.network, module.eks]
}
